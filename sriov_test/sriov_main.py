#!/usr/bin/env python3

import os
import sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(parent)
import time
import inspect
import ethtool
from plumbum import local
from bash import bash
from common.beaker_cmd import (i_am_client,i_am_server)
from common.beaker_cmd import (enter_phase, log, log_and_run)
from common.beaker_cmd import (pushd, run,set_check)
from common.beaker_cmd import (rl_fail,sync_set,sync_wait)
from common.lib_sriov import LIB_SRIOV as pysriov
from common import tools
from common import xmltool
from common.lib_iperf_all import vm_run_cmd
from common.lib_iperf_all import do_host_iperf
from common.lib_iperf_all import do_vm_iperf

case_path = os.environ.get("CASE_PATH")
system_version_id = int(os.environ.get("SYSTEM_VERSION_ID"))
my_tool = tools.Tools()
xml_tool = xmltool.XmlTool()
image_dir = "/var/tmp/"
client_target = "CLIENT"
server_target = "SERVER"
vm1 = "g1"
vm2 = "g2"
vm3 = "g3"
###############################################################################################
###############################################################################################
###############################################################################################
###############################################################################################
def get_env(var_name):
    return os.environ.get(var_name)

def sriov_get_ipaddr():
    if get_env("JOBID") == "" or None == get_env("JOBID"):
        ipaddr = 214
        ipaddr_vlan = 213
    else:
        ipaddr = int(get_env("JOBID")) % 100  + 20
        ipaddr_vlan = ipaddr + 50
    return ipaddr,ipaddr_vlan

nic_test = get_env("NIC_TEST")
print(f"nic_test : {nic_test}")
ipaddr,ipaddr_vlan = sriov_get_ipaddr()
print(f"ipaddr :{ipaddr} ipaddr_vlan : {ipaddr_vlan}")
if i_am_server():
    mac4vm1 = f"00:de:ad:{ipaddr:02x}:00:01"
    print(f"mac4vm1 is: {mac4vm1}")
    mac4vm2 = f"00:de:ad:{ipaddr:02x}:00:02"
    print(f"mac4vm2 is: {mac4vm2}")
    mac4vm1if2 = f"00:de:ad:{ipaddr:02x}:00:03"
    print(f"mac4vm1if2 is: {mac4vm1if2}")
else:
    mac4vm1 = f"00:de:ad:{ipaddr:02x}:00:21"
    print(f"mac4vm1 is: {mac4vm1}")
    mac4vm2 = f"00:de:ad:{ipaddr:02x}:00:22"
    print(f"mac4vm2 is: {mac4vm2}")
    mac4vm1if2 = f"00:de:ad:{ipaddr:02x}:00:23"
    print(f"mac4vm1if2 is: {mac4vm1if2}")

def py3_run(cmd,str_ret_val="0"):
    run(f"source {case_path}/venv/bin/activate")
    run(cmd,str_ret_val)
    run("deactivate")
    pass

#################################################################################
#################################################################################
#################################################################################
#################################################################################
# Get test mode for single port or two ports
def is_single_port_mode():
    port_mode = get_env("SINGLE_PORT_MODE")
    if "true" in port_mode.lower():
        return True
    else:
        return False

def download_VNF_image():
    cmd = f"""
    chmod 777 {image_dir}
    """
    log_and_run(cmd)
    with pushd(case_path):
        one_queue_image = get_env("ONE_QUEUE_IMAGE")
        two_queue_image = get_env("TWO_QUEUE_IMAGE")
        one_queue_image_name = os.path.basename(one_queue_image)
        two_queue_image_name = os.path.basename(two_queue_image)
        one_queue_image_backup_name = "backup_" + one_queue_image_name
        two_queue_image_backup_name = "backup_" + two_queue_image_name
        #for one queue image backup
        if not os.path.exists(f"{image_dir}/{one_queue_image_backup_name}"):
            log_info = """
            ***********************************************************************
            Downloading and decompressing VNF image. This may take a while!
            ***********************************************************************
            """
            log(log_info)
            cmd = f"""
            wget  {one_queue_image} -O {image_dir}/{one_queue_image_backup_name} > /dev/null 2>&1
            """
            log_and_run(cmd)

        #for two queue image backup
        if not os.path.exists(f"{image_dir}/{two_queue_image_backup_name}"):
            log_info = """
            ***********************************************************************
            Downloading and decompressing VNF image. This may take a while!
            ***********************************************************************
            """
            log(log_info)
            cmd = f"""
            wget  {two_queue_image} -O {image_dir}/{two_queue_image_backup_name}> /dev/null 2>&1
            """
            log_and_run(cmd)

        #config a new image from backup image
        if os.path.exists(f"{image_dir}/{one_queue_image_name}"):
            with pushd(f"{image_dir}"):
                cmd = f"""
                rm -f {one_queue_image_name}
                cp {one_queue_image_backup_name} {one_queue_image_name}
                """
                log_and_run(cmd)
        else:
            with pushd(f"{image_dir}"):
                cmd = f"""
                cp {one_queue_image_backup_name} {one_queue_image_name}
                """
                log_and_run(cmd)

        #config a new two queue image from backup image
        if os.path.exists(f"{image_dir}/{two_queue_image_name}"):
            with pushd(f"{image_dir}"):
                cmd = f"""
                rm -f {two_queue_image_name}
                cp {two_queue_image_backup_name} {two_queue_image_name}
                """
                log_and_run(cmd)
        else:
            with pushd(f"{image_dir}"):
                cmd = f"""
                cp {two_queue_image_backup_name} {two_queue_image_name}
                """
                log_and_run(cmd)

        udev_file = "60-persistent-net.rules"
        data = """
        ACTION=="add", SUBSYSTEM=="net", KERNELS=="0000:03:00.0", NAME:="eth1"
        ACTION=="add", SUBSYSTEM=="net", KERNELS=="0000:04:00.0", NAME:="eth2"
        """
        log("add net rules to guest image")
        log(data)
        local.path(udev_file).write(data)


    cmd = f"""
    virt-copy-in -a {image_dir}/{one_queue_image_name} {udev_file} /etc/udev/rules.d/
    virt-copy-in -a {image_dir}/{two_queue_image_name} {udev_file} /etc/udev/rules.d/
    """
    log_and_run(cmd)

    dpdk_url = get_env("DPDK_URL")
    dpdk_tool_url = get_env("DPDK_TOOL_URL")
    dpdk_ver = get_env("DPDK_VER")

    cmd =  f"""
    rm -rf /root/{dpdk_ver}
    mkdir -p /root/{dpdk_ver}
    wget -P /root/{dpdk_ver}/ {dpdk_url} > /dev/null 2>&1
    wget -P /root/{dpdk_ver}/ {dpdk_tool_url} > /dev/null 2>&1
    virt-copy-in -a {image_dir}/{one_queue_image_name} /root/{dpdk_ver} /root/
    virt-copy-in -a {image_dir}/{two_queue_image_name} /root/{dpdk_ver} /root/
    sleep 5
    """
    log_and_run(cmd)

    #copy driverctl into guest image
    driverctl_dir="/root/driverctl_dir/"
    cmd = f"""
    rm -rf {driverctl_dir}
    mkdir -p {driverctl_dir}
    dnf download driverctl --destdir={driverctl_dir}
    virt-copy-in -a {image_dir}/{one_queue_image_name} {driverctl_dir} /root/
    virt-copy-in -a {image_dir}/{two_queue_image_name} {driverctl_dir} /root/
    sleep 10
    """
    log_and_run(cmd)

    return 0


def vcpupin_in_xml(numa_node, template_xml, new_xml, cpu_list):
    with pushd(case_path):
        local.path(template_xml).copy(new_xml)
        xml_tool.xml_add_vcpupin_item(new_xml, len(cpu_list))
        xml_tool.update_numa(new_xml,numa_node)
        for i in range(len(cpu_list)):
            xml_tool.update_vcpu(new_xml, i, cpu_list[i])
    return 0

def start_guest(guest_xml):
    with pushd(case_path):
        run("systemctl list-units --state=stop --type=service | grep libvirtd || systemctl restart libvirtd")
        download_VNF_image()
        cmd = f"""
        virsh define {case_path}/{guest_xml}
        virsh start gg
        """
        run(cmd)
    return 0

def destroy_guest():
    cmd = """
    virsh destroy gg
    virsh undefine gg
    """
    run(cmd)
    return 0

def configure_guest():
    cmd = """
    stty rows 24 cols 120
    test -d /sys/class/net/eth1/ && nmcli dev set eth1 managed no
    test -d /sys/class/net/eth2/ && nmcli dev set eth2 managed no
    systemctl stop firewalld
    iptables -t filter -P INPUT ACCEPT
    iptables -t filter -P FORWARD ACCEPT
    iptables -t filter -P OUTPUT ACCEPT
    iptables -t mangle -P PREROUTING ACCEPT
    iptables -t mangle -P INPUT ACCEPT
    iptables -t mangle -P FORWARD ACCEPT
    iptables -t mangle -P OUTPUT ACCEPT
    iptables -t mangle -P POSTROUTING ACCEPT
    iptables -t nat -P PREROUTING ACCEPT
    iptables -t nat -P INPUT ACCEPT
    iptables -t nat -P OUTPUT ACCEPT
    iptables -t nat -P POSTROUTING ACCEPT
    iptables -t filter -F
    iptables -t filter -X
    iptables -t mangle -F
    iptables -t mangle -X
    iptables -t nat -F
    iptables -t nat -X
    ip6tables -t filter -P INPUT ACCEPT
    ip6tables -t filter -P FORWARD ACCEPT
    ip6tables -t filter -P OUTPUT ACCEPT
    ip6tables -t mangle -P PREROUTING ACCEPT
    ip6tables -t mangle -P INPUT ACCEPT
    ip6tables -t mangle -P FORWARD ACCEPT
    ip6tables -t mangle -P OUTPUT ACCEPT
    ip6tables -t mangle -P POSTROUTING ACCEPT
    ip6tables -t nat -P PREROUTING ACCEPT
    ip6tables -t nat -P INPUT ACCEPT
    ip6tables -t nat -P OUTPUT ACCEPT
    ip6tables -t nat -P POSTROUTING ACCEPT
    ip6tables -t filter -F
    ip6tables -t filter -X
    ip6tables -t mangle -F
    ip6tables -t mangle -X
    ip6tables -t nat -F
    ip6tables -t nat -X
    ip -d addr show
    """
    pts = bash("virsh ttyconsole gg").value()
    ret = my_tool.run_cmd_get_output(pts, cmd)
    log(ret)
    return 0

def check_guest_testpmd_result():
    cmd = f"""
    show port info all
    show port stats all
    """
    pts = bash("virsh ttyconsole gg").value()
    ret = my_tool.run_cmd_get_output(pts, cmd,"testpmd>")
    log(ret)
    return 0

#rpm -ivh /root/dpdkrpms/{dpdk_ver}/dpdk*.rpm
# {modprobe  vfio enable_unsafe_noiommu_mode=1}
def guest_start_testpmd(queue_num, guest_cpu_list, rxd_size, txd_size,max_pkt_len,fwd_mode):
    dpdk_ver = get_env("DPDK_VER")
    cmd = fr"""
    stty rows 24 cols 120
    /root/one_gig_hugepages.sh 1
    rpm -ivh /root/{dpdk_ver}/dpdk*.rpm
    rpm -q driverctl || yum install -y driverctl
    rpm -q driverctl || rpm -ivh /root/driverctl_dir/driverctl*.rpm
    # for i in `ls /root/{dpdk_ver}/`; do rpm -ivh /root/{dpdk_ver}/$i; done
    echo "options vfio enable_unsafe_noiommu_mode=1" > /etc/modprobe.d/vfio.conf
    modprobe -r vfio_iommu_type1
    modprobe -r vfio-pci
    modprobe -r vfio
    modprobe  vfio
    modprobe vfio-pci
    test -d /sys/class/net/eth1/ && ip link set eth1 down
    test -d /sys/class/net/eth2/ && ip link set eth2 down
    ip -d link show
    driver=$(lspci -s 0000:03:00.0 -v | grep Kernel | grep modules | awk '{{print $NF}}')
    sleep 3
    echo "Diver is "$driver
    grep "mlx" <<< $driver || ( test -d /sys/bus/pci/devices/0000:03:00.0/ && driverctl -v set-override 0000:03:00.0 vfio-pci )
    grep "mlx" <<< $driver || ( test -d /sys/bus/pci/devices/0000:04:00.0/ && driverctl -v set-override 0000:04:00.0 vfio-pci )
    grep "mlx" <<< $driver && ( test -d /sys/bus/pci/devices/0000:03:00.0/ && driverctl -v unset-override 0000:03:00.0 )
    grep "mlx" <<< $driver && ( test -d /sys/bus/pci/devices/0000:04:00.0/ && driverctl -v unset-override 0000:04:00.0 )
    driverctl -v list-overrides
    """
    pts = bash("virsh ttyconsole gg").value()
    ret = my_tool.run_cmd_get_output(pts, cmd)
    print("**********************************")
    print(ret)
    print("**********************************")

    num_core = 2
    if queue_num == 1:
        num_core = 2
    else:
        num_core = 4

    hw_vlan_flag = ""
    legacy_mem = ""

    dpdk_version = int(get_env("DPDK_VER").split('-')[0])
    if dpdk_version >= 1811:
        legacy_mem = " --legacy-mem "
        hw_vlan_flag = ""
    else:
        legacy_mem = ""
        hw_vlan_flag = "--disable-hw-vlan"
    
    extra_parameter = ""

    if dpdk_version >= 2011:
        testpmd_cmd = "dpdk-testpmd"
    else:
        testpmd_cmd = "testpmd"

    if is_single_port_mode():
        port_topo="chained"
    else:
        port_topo="paired"

    cmd_test = f"""{testpmd_cmd} -l {guest_cpu_list} \
    --socket-mem 1024 \
    {legacy_mem} \
    -n 4 \
    -- \
    --burst=64 \
    --forward-mode={fwd_mode} \
    --port-topology={port_topo} \
    {hw_vlan_flag} \
    --disable-rss \
    -i \
    --rxq={queue_num} \
    --txq={queue_num} \
    --rxd={rxd_size} \
    --txd={txd_size} \
    --nb-cores={num_core} \
    --max-pkt-len={int(max_pkt_len) + 4} \
    {extra_parameter} \
    --auto-start
    """
    log(cmd_test)
    ret = my_tool.run_cmd_get_output(pts,cmd_test,"testpmd>")
    print("***********************************")
    print(ret)
    print("***********************************")
    return 0

@set_check(0)
def clear_dpdk_interface():
    bus_list = bash(r"driverctl -v list-devices|grep \*").value()
    print(bus_list)
    for i in str(bus_list).split(os.linesep):
        if len(i.strip()) > 0:
            pci_bus = i.split()[0]
            run(f"driverctl -v unset-override {pci_bus}")
    pass

@set_check(0)
def clear_hugepage():
    hugepage_dir = bash("mount -l | grep hugetlbfs | awk '{print $3}'").value()
    log_and_run(f"rm -rf {hugepage_dir}/*")
    return 0

def clear_env():
    cmd = """
    systemctl start openvswitch
    ovs-vsctl --if-exists del-br ovsbr0
    virsh destroy gg
    virsh undefine gg
    systemctl stop openvswitch
    ip tuntap del tap0 mode tap
    ip tuntap del tap1 mode tap
    """
    run(cmd,"0,1")
    clear_dpdk_interface()
    clear_hugepage()
    log_and_run("sleep 10")
    log_and_run("ip link show")
    return 0

def bonding_test_trex_single_port(t_time,pkt_size,dst_mac):
    trex_server_ip = get_env("TRAFFICGEN_TREX_HOST_IP_ADDR")
    trex_url = get_env("TREX_URL")
    trex_dir = os.path.basename(trex_url).replace(".tar.gz","")
    trex_name = os.path.basename(trex_url)

    with pushd(case_path):
        ret = bash(f"ping {trex_server_ip} -c 3")
        if ret.code != 0:
            log("Trex server {} not up please check ".format(trex_server_ip))
        cmd = fr"""
        [ -f {trex_name} ] || wget -nv -N --no-check-certificate {trex_url};tar xf {trex_name};ln -sf {trex_dir} current; ls -l;
        """
        log_and_run(cmd)
        import time
        time.sleep(3)
        cmd = f""" python -u ./trex_sport.py -c {trex_server_ip} -d {dst_mac} -t {t_time} --pkt_size={pkt_size} -m 10 """
        py3_run(cmd)
    return 0

#Wtih binary_search version
def bonding_test_trex(t_time,pkt_size,dst_mac_one,dst_mac_two):
    trex_server_ip = get_env("TRAFFICGEN_TREX_HOST_IP_ADDR")
    trex_url = get_env("TREX_URL")
    trex_dir = os.path.basename(trex_url).replace(".tar.gz","")
    trex_name = os.path.basename(trex_url)
    #init trex package and lua traffic generator
    # [ -e trafficgen ] || git clone https://github.com/atheurer/trafficgen.git
    # [ -e trafficgen ] || git clone https://github.com/wanghekai/trafficgen.git
    # Here have a little modify for binary-search to adapt mellanox support
    # just modify negative-packets-loss to pass as default.
    with pushd("/opt"):
        cmd = fr"""
        [ -e trafficgen ] || git clone https://github.com/wanghekai/trafficgen.git
        mkdir -p trex
        pushd trex &>/dev/null
        [ -f {trex_name} ] || wget -nv -N --no-check-certificate {trex_url};tar xf {trex_name};ln -sf {trex_dir} current; ls -l;
        popd &>/dev/null
        chmod 777 /opt/trex -R
        """
        log_and_run(cmd)
        pass
    with pushd(case_path):
        ret = bash(f"ping {trex_server_ip} -c 3")
        if ret.code != 0:
            log("Trex server {} not up please check ".format(trex_server_ip))
        pass
    with pushd("/opt/trafficgen"):
        import sys
        sys.path.append('/opt/trex/current/automation/trex_control_plane/interactive')
        import json
        from trex.stl.api import STLClient
        from trex.stl.api import TRexError
        # from trex_tg_lib import *
        c = STLClient(server = trex_server_ip)
        try:
            # connect to server
            print("Establishing connection to TRex server...")
            c.connect()
            print("Connection established")

            # prepare our ports
            c.acquire(ports = [0], force=True)
            c.reset(ports = [0])

            port_info = c.get_port_info(ports = [0])
            #port_info[0]["driver"]
            print(port_info)
        except TRexError as e:
            print(e)
        finally:
            c.disconnect()
        trex_port_driver_info = port_info[0]["driver"]
        if "mlx" in trex_port_driver_info:
            print("Trex driver is Mellanox..........................")
            cmd = f"""
            python ./binary-search.py \
            --trex-host={trex_server_ip} \
            --traffic-generator=trex-txrx \
            --frame-size={pkt_size} \
            --traffic-direction=bidirectional \
            --search-runtime={t_time} \
            --search-granularity=0.1 \
            --validation-runtime=600 \
            --negative-packet-loss=pass \
            --max-loss-pct=0.0 \
            --rate-unit=% \
            --rate=100 \
            --use-device-stats \
            --dst-macs={dst_mac_one},{dst_mac_two}
            """
        else:
            cmd = f"""
            python ./binary-search.py \
            --trex-host={trex_server_ip} \
            --traffic-generator=trex-txrx \
            --frame-size={pkt_size} \
            --traffic-direction=bidirectional \
            --search-runtime={t_time} \
            --search-granularity=0.1 \
            --validation-runtime=600 \
            --negative-packet-loss=fail \
            --max-loss-pct=0.0 \
            --rate-unit=% \
            --rate=100 \
            --dst-macs={dst_mac_one},{dst_mac_two}
            """
        log(cmd)
        py3_run(cmd)
    return 0


def attach_sriov_vf_to_vm(xml_file,vm,vlan_id=0):
    vf1_bus_info = my_tool.get_bus_from_name(get_env("NIC1_VF"))
    vf1_bus_info = vf1_bus_info.replace(":",'_')
    vf1_bus_info = vf1_bus_info.replace(".",'_')
    log(vf1_bus_info)
    vf1_domain = vf1_bus_info.split('_')[0]
    vf1_bus    = vf1_bus_info.split('_')[1]
    vf1_slot   = vf1_bus_info.split('_')[2]
    vf1_func   = vf1_bus_info.split('_')[3]

    if False == is_single_port_mode():
        vf2_bus_info = my_tool.get_bus_from_name(get_env("NIC2_VF"))
        vf2_bus_info = vf2_bus_info.replace(":",'_')
        vf2_bus_info = vf2_bus_info.replace(".",'_')
        log(vf2_bus_info)
        vf2_domain = vf2_bus_info.split('_')[0]
        vf2_bus    = vf2_bus_info.split('_')[1]
        vf2_slot   = vf2_bus_info.split('_')[2]
        vf2_func   = vf2_bus_info.split('_')[3]

    vlan_item = """
    <interface type='hostdev' managed='yes'>
        <mac address='{}'/>
        <vlan >
            <tag id='{}'/>
        </vlan>
        <driver name='vfio'/>
        <source >
            <address type='pci' domain='0x{}' bus='0x{}' slot='0x{}' function='0x{}'/>
        </source >
        <address type='pci' domain='{}' bus='{}' slot='{}' function='{}'/>
    </interface >
    """

    item = """
    <interface type='hostdev' managed='yes'>
        <mac address='{}'/>
        <driver name='vfio'/>
        <source >
            <address type='pci' domain='0x{}' bus='0x{}' slot='0x{}' function='0x{}'/>
        </source >
        <address type='pci' domain='{}' bus='{}' slot='{}' function='{}'/>
    </interface >
    """

    vf1_xml_path = os.getcwd() + "/vf1.xml"
    if os.path.exists(vf1_xml_path):
        os.remove(vf1_xml_path)
    local.path(vf1_xml_path).touch()
    vf1_f_obj = local.path(vf1_xml_path)

    if False == is_single_port_mode():
        vf2_xml_path = os.getcwd() + "/vf2.xml"
        if os.path.exists(vf2_xml_path):
            os.remove(vf2_xml_path)
        local.path(vf2_xml_path).touch()
        vf2_f_obj = local.path(vf2_xml_path)
    
    import xml.etree.ElementTree as xml

    if vlan_id != 0:
        vf1_format_list = ['52:54:00:11:8f:ea', vlan_id ,vf1_domain ,vf1_bus, vf1_slot, vf1_func, '0x0000', '0x03', '0x0', '0x0']
        vf1_vlan_item = vlan_item.format(*vf1_format_list)
        vf1_vlan_obj = xml.fromstring(vf1_vlan_item)
        vf1_f_obj.write(xml.tostring(vf1_vlan_obj))
        if False == is_single_port_mode():
            vf2_format_list = ['52:54:00:11:8f:eb', vlan_id, vf2_domain, vf2_bus, vf2_slot, vf2_func, '0x0000', '0x04', '0x0', '0x0']
            vf2_vlan_item = vlan_item.format(*vf2_format_list)
            vf2_vlan_obj = xml.fromstring(vf2_vlan_item)
            vf2_f_obj.write(xml.tostring(vf2_vlan_obj))
    else:
        vf1_format_list = ['52:54:00:11:8f:ea' ,vf1_domain, vf1_bus, vf1_slot, vf1_func, '0x0000', '0x03', '0x0', '0x0']
        vf1_novlan_item = item.format(*vf1_format_list)
        vf1_novlan_obj = xml.fromstring(vf1_novlan_item)
        vf1_f_obj.write(xml.tostring(vf1_novlan_obj))
        if False == is_single_port_mode():
            vf2_format_list = ['52:54:00:11:8f:eb' ,vf2_domain ,vf2_bus, vf2_slot ,vf2_func, '0x0000', '0x04', '0x0', '0x0']
            vf2_novlan_item = item.format(*vf2_format_list)
            vf2_novlan_obj = xml.fromstring(vf2_novlan_item)
            vf2_f_obj.write(xml.tostring(vf2_novlan_obj))

    if is_single_port_mode():
        cmd = f"""
        sleep 10
        echo "#################################################"
        cat {vf1_xml_path}
        echo "#################################################"
        virsh attach-device {vm} {vf1_xml_path}
        sleep 5
        virsh dumpxml {vm}
        """
        log_and_run(cmd)
    else:
        cmd = f"""
        sleep 10
        echo "#################################################"
        cat {vf1_xml_path}
        echo "#################################################"
        cat {vf2_xml_path}
        echo "#################################################"
        virsh attach-device {vm} {vf1_xml_path}
        sleep 5
        virsh dumpxml {vm}
        sleep 10
        virsh attach-device {vm} {vf2_xml_path}
        sleep 5
        virsh dumpxml {vm}
        """
        log_and_run(cmd)

    return 0


def sriov_pci_passthrough_test_single_port(q_num,pkt_size,cont_time):
    clear_env()
    numa_node = bash("cat /sys/class/net/{}/device/numa_node".format(get_env("NIC1_VF"))).value()
    vcpu_list = [ get_env("VCPU1"),get_env("VCPU2"),get_env("VCPU3")]
    if q_num != 1:
        vcpu_list = [ get_env("VCPU1"),get_env("VCPU2"),get_env("VCPU3"),get_env("VCPU4"),get_env("VCPU5")]
    new_xml = "g1.xml"
    vcpupin_in_xml(numa_node,"guest.xml",new_xml,vcpu_list)
    #clear the old hostdev config and update xml file
    xml_tool.remove_item_from_xml(new_xml,"./devices/interface[@type='hostdev']")

    one_queue_image_name = os.path.basename(get_env("ONE_QUEUE_IMAGE"))
    two_queue_image_name = os.path.basename(get_env("TWO_QUEUE_IMAGE"))

    if q_num == 1:
        xml_tool.update_image_source(new_xml,image_dir + "/" + one_queue_image_name)
    else:
        xml_tool.update_image_source(new_xml,image_dir + "/" + two_queue_image_name)

    start_guest(new_xml)

    #Here attach vf to vm
    attach_sriov_vf_to_vm(new_xml,"gg")

    configure_guest()

    log("guest start testpmd test Now")
    if q_num == 1:
        guest_cpu_list="0,1,2"
    else:
        guest_cpu_list="0,1,2,3,4"

    guest_start_testpmd(q_num,guest_cpu_list,get_env("SRIOV_RXD_SIZE"),get_env("SRIOV_TXD_SIZE"),pkt_size,"macswap")

    log("sriov pci passthrough PVP performance test Begin Now")
    bonding_test_trex_single_port(cont_time,pkt_size,"52:54:00:11:8f:ea")

    check_guest_testpmd_result()

    return 0


def sriov_pci_passthrough_test(q_num,pkt_size,cont_time):
    clear_env()
    numa_node = bash("cat /sys/class/net/{}/device/numa_node".format(get_env("NIC1_VF"))).value()
    vcpu_list = [ get_env("VCPU1"),get_env("VCPU2"),get_env("VCPU3")]
    if q_num != 1:
        vcpu_list = [ get_env("VCPU1"),get_env("VCPU2"),get_env("VCPU3"),get_env("VCPU4"),get_env("VCPU5")]
    new_xml = "g1.xml"
    vcpupin_in_xml(numa_node,"guest.xml",new_xml,vcpu_list)
    #clear the old hostdev config and update xml file
    xml_tool.remove_item_from_xml(new_xml,"./devices/interface[@type='hostdev']")
    # Here because of the limit of p35 archtechture , I can not add two vf into vm at the same time
    # So , make a workaround , add vf with virsh attach-device two times

    one_queue_image_name = os.path.basename(get_env("ONE_QUEUE_IMAGE"))
    two_queue_image_name = os.path.basename(get_env("TWO_QUEUE_IMAGE"))

    if q_num == 1:
        xml_tool.update_image_source(new_xml,image_dir + "/" + one_queue_image_name)
    else:
        xml_tool.update_image_source(new_xml,image_dir + "/" + two_queue_image_name)


    start_guest(new_xml)

    #Here attach vf to vm 
    attach_sriov_vf_to_vm(new_xml,"gg")

    configure_guest()

    log("guest start testpmd test Now")
    if q_num == 1:
        guest_cpu_list="0,1,2"
    else:
        guest_cpu_list="0,1,2,3,4"

    guest_start_testpmd(q_num,guest_cpu_list,get_env("SRIOV_RXD_SIZE"),get_env("SRIOV_TXD_SIZE"),pkt_size,"io")

    log("sriov pci passthrough PVP performance test Begin Now")
    bonding_test_trex(cont_time,pkt_size,"52:54:00:11:8f:ea","52:54:00:11:8f:eb")

    check_guest_testpmd_result()

    return 0

def sriov_pci_passthrough_test_wrap(q_num,pkt_size,cont_time):
    pmd_num = q_num * 2
    with enter_phase(f"SRIOV-VF-PCI-PASSTHROUGH-{pkt_size}-Bytes-{q_num}Q-{pmd_num}PMD-TEST"):
        if is_single_port_mode():
            sriov_pci_passthrough_test_single_port(q_num,pkt_size,cont_time)
        else:
            sriov_pci_passthrough_test(q_num,pkt_size,cont_time)
        pass
    pass

def vm_restart_iperf(vm_name):
    cmd = f"""
    pid_iperf=$(pidof iperf)
    kill $pid_iperf 2>/dev/null
    wait $pid_iperf  2>/dev/null
    iperf -s -p 8080 -D &
    """
    vm_run_cmd(vm_name,cmd)
    pass

def host_restart_iperf():
    cmd = f"""
    pid_iperf=$(pidof iperf)
    kill $pid_iperf  2>/dev/null
    wait $pid_iperf  2>/dev/null
    iperf -s -p 8080 -D &
    """
    run(cmd,"0,127")
    pass


def ignore_NM_Manager_devices():
    unmanage_device_conf_file = "/etc/NetworkManager/conf.d/99-unmanaged-devices.conf"
    cmd = f"""
    rm -rf {unmanage_device_conf_file}
    """
    run(cmd)
    default_nic = my_tool.get_default_iface()
    conf_data = f"""
    [keyfile]
    unmanaged-devices=*,except:interface-name:{default_nic},except:type:loopback,except:type:bridge
    """
    local.path(unmanage_device_conf_file).touch()
    local.path(unmanage_device_conf_file).write(conf_data)
    cmd = f"""
    systemctl reload NetworkManager
    """
    run(cmd)
    pass

def sriov_install():
    iperf_pid = bash("pidof iperf").value()
    if len(iperf_pid) > 0:
        cmd = f"""
        kill {iperf_pid}  2>/dev/null 
        wait {iperf_pid}  2>/dev/null
        """
        run(cmd,"0,127")
    cmd = f"""
    which iperf && iperf -s -p 8080 -D &
    """
    run(cmd)
    pass

# this will be called before and after the test
def sriov_cleanup():
    str_path = "/sys/class/net/"
    path = local.path(str_path)
    for i in path:
        if local.path(str_path + i.name + "/bridge/").exists():
            cmd = f"""
            ip link set {str(i.name)} down
            ip link del {str(i.name)}
            """
            run(cmd)
    cmd = f"""
    virsh list --all --name | xargs -I {{}} virsh destroy {{}}
    virsh list --all --name | xargs -I {{}} virsh undefine {{}}
    virsh net-list --all --name | xargs -I {{}} virsh net-destroy {{}}
    virsh net-list --all --name | xargs -I {{}} virsh net-undefine {{}}
    """
    run(cmd,"0,123")
    cmd = f"""
    pkill dhclient
    sleep 5
    """
    run(cmd,"0,1")
    default_nic = my_tool.get_default_iface()
    cmd = f"""
    ip link set dev {default_nic} nomaster
    dhclient {default_nic}
    """
    run(cmd)
    pass

def enable_libvirtd_as_default_rhel9():
    drv_list = ["qemu","interface","network","nodedev","nwfilter","secret","storage","proxy"]
    for drv in drv_list:
        cmd = f"""
        systemctl unmask virt{drv}d.service
        systemctl unmask virt{drv}d{{,-ro,-admin}}.socket
        systemctl enable virt{drv}d.service
        systemctl is-enabled virt{drv}d.service
        systemctl enable virt{drv}d{{,-ro,-admin}}.socket
        systemctl is-enabled virt{drv}d{{,-ro,-admin}}.socket
        systemctl start virt{drv}d.service
        systemctl status virt{drv}d.service --no-pager -l
        """
        run(cmd)
    cmd = f"""
    systemctl unmask libvirtd.service
    systemctl enable libvirtd.service
    systemctl is-enabled libvirtd.service
    systemctl unmask libvirtd{{,-ro,-admin}}.socket
    systemctl enable libvirtd{{,-ro,-admin}}.socket
    systemctl is-enabled libvirtd{{,-ro,-admin}}.socket
    """
    run(cmd)
    pass

# set up env
def sriov_setup():
    IMG_GUEST = get_env("ONE_QUEUE_IMAGE")
    cmd = f"""
    iptables -F
    ip6tables -F
    systemctl stop firewalld
    systemctl disable firewalld
    #show bridge list
    ip link show type bridge
    ip addr list
    echo "Download guest image..."
    pushd "/var/lib/libvirt/images/" 1>/dev/null
    [ -e "{IMG_GUEST}" ] || wget -nv -N -c -t 3 {IMG_GUEST}
    cp --remove-destination $(basename {IMG_GUEST}) {vm1}.qcow2
    cp --remove-destination $(basename {IMG_GUEST}) {vm2}.qcow2
    ls
    popd 1>/dev/null
    virsh net-list --all | grep default || virsh net-define /usr/share/libvirt/networks/default.xml
    virsh net-list  | grep default || virsh net-start default
    virsh net-autostart default
    ip link show | grep virbr1 || ip link add name virbr1 type bridge
    ip link set virbr1 up
    virt-install \
        --name {vm1} \
        --vcpus=2 \
        --ram=2048 \
        --disk path=/var/lib/libvirt/images/{vm1}.qcow2,device=disk,bus=virtio,format=qcow2 \
        --network bridge=virbr0,model=virtio,mac={mac4vm1} \
        --network bridge=virbr1,model=virtio,mac={mac4vm1if2} \
        --import --boot hd \
        --accelerate \
        --graphics vnc,listen=0.0.0.0 \
        --force \
        --os-variant=rhel-unknown \
        --noautoconsole
    virt-install \
        --name {vm2} \
        --vcpus=2 \
        --ram=2048 \
        --disk path=/var/lib/libvirt/images/{vm2}.qcow2,device=disk,bus=virtio,format=qcow2 \
        --network bridge=virbr0,model=virtio,mac={mac4vm2} \
        --import --boot hd \
        --accelerate \
        --graphics vnc,listen=0.0.0.0 \
        --force \
        --os-variant=rhel-unknown \
        --noautoconsole
    sleep 90
    """
    run(cmd)

    cmd = f"""
    iptables -F
    ip6tables -F
    systemctl stop firewalld
    setenforce 0
    """
    vm_run_cmd(vm1,cmd)
    vm_run_cmd(vm2,cmd)

    cmd = f"""
    yum install -yq wget
    yum install -yq tcpdump
    yum install -yq bzip2
    yum install -yq gcc
    yum install -yq automake
    yum install -yq nmap-ncat
    tshark -v || yum -yq install wireshark
    yum -yq install iperf3
    yum -yq install python3-pip
    """
    network_cmd = f"""
    systemctl stop NetworkManager
    """
    if system_version_id >= 70:
        cmd = cmd + network_cmd
    vm_run_cmd(vm1,cmd)
    vm_run_cmd(vm2,cmd)

    cmd = f"""
    iperf -s -p 8080 -D &
    """
    vm_run_cmd(vm1,cmd)
    vm_run_cmd(vm2,cmd)
    pass

def setup():
    ignore_NM_Manager_devices()
    sriov_install()
    sriov_cleanup()
    if system_version_id >= 90:
        enable_libvirtd_as_default_rhel9()
    sriov_setup()
    pass

def iperf_kill_and_start():
    sriov_install()
    pass

def vm_iperf_kill_and_start(vm_name):
    cmd = f"""
    vm_ieprf_id=`pidof iperf`
    kill $vm_ieprf_id 2>/dev/null
    wait $vm_ieprf_id 2>/dev/null
    which iperf && iperf -s -p 8080 -D &
    """
    vm_run_cmd(vm_name,cmd)
    pass


def vmpreconfig():
    cmd = f"""
    ip link show | grep virbr1 || ip link add name virbr1 type bridge
    ip link set virbr1 up

    ip link show virbr0 || {{ \
    virsh net-list --all \
    virsh net-destroy default \
    virsh net-autostart default \
    virsh net-start default

    if (( {system_version_id} < 90 ));then \
        systemctl restart libvirtd \
    else \
        systemctl restart virtnetworkd.service \
        systemctl status virtqemud --no-pager -l \
    fi
    virsh net-list --all
    sleep 10
    ip link show virbr0
    }}
    """
    run(cmd)
    iperf_kill_and_start()
    cmd = f"""
    #restart vm if needed
    virsh list --all | grep {vm1} | grep running || virsh start {vm1}
    virsh list --all | grep {vm2} | grep running || virsh start {vm2}
    sleep 10
    """
    vm_iperf_kill_and_start(vm1)
    vm_iperf_kill_and_start(vm2)
    if system_version_id >= 70:
        vm_run_cmd(vm1,"systemctl stop NetworkManager")
        vm_run_cmd(vm2,"systemctl stop NetworkManager")
    pass

def sriov_deatch_vf_from_vm(vm,vf_bus):
    import re
    vf_bus = re.sub("[.:]", "_", vf_bus)
    vf_nodedev = "pci_" + vf_bus + ".xml"
    pysriov.sriov_detach_vf_from_vm(vm,vf_nodedev)
    pass

# create vf from pf nic name with special num
@set_check(0)
def vf_create(pf, num):
    run(f"ip li set {pf} up")
    pf_bus = pysriov.sriov_get_pf_bus_from_pf_name(pf)
    cmds = pysriov.sriov_create_vfs(pf_bus[0], num)
    log(cmds)
    # for some nic create speed is low so here wait some time
    time.sleep(5)
    pass

@set_check(0)
def vf_create_from_pf(pf_name, num):
    vf_create(pf_name, num)
    vf_list = pysriov.sriov_get_vf_list(pf_name)
    return vf_list

def sriov_test_pf_remote():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    log(f"PF({nic_test}) <---> REMOTE")
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    """
    run(cmd)
    if i_am_server():
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_wait(client_target,sync_start)
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        cmd = f"""
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr}.1/24 dev {nic_test}
        ip addr add 2021:db8:{ipaddr}::1/64 dev {nic_test}
        ip -d link show {nic_test}
        ip -d addr show {nic_test}
        """
        run(cmd)
        sync_wait(client_target,sync_start)

        do_host_iperf(f"172.30.{ipaddr}.2",f"2021:db8:{ipaddr}::2")

        sync_set(client_target,sync_end)

        cmd = f"""
        ip addr flush {nic_test}
        """
        run(cmd)
        vf_create_from_pf(nic_test,0)
    else:
        cmd = f"""
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr}.2/24 dev {nic_test}
        ip addr add 2021:db8:{ipaddr}::2/64 dev {nic_test}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip addr flush {nic_test}
        """
        run(cmd)
    pass

def sriov_test_pf_remote_jumbo():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    log(f"{func_name}")
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    """
    run(cmd)

    if i_am_server():
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_wait(client_target,sync_start)
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        cmd = f"""
        ip link set mtu 9000 dev {nic_test}
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr}.1/24 dev {nic_test}
        ip addr add 2021:db8:{ipaddr}::1/64 dev {nic_test}
        ip -d link show {nic_test}
        ip -d addr show {nic_test}
        """
        run(cmd)
        sync_wait(client_target,sync_start)

        do_host_iperf(f"172.30.{ipaddr}.2",f"2021:db8:{ipaddr}::2")

        sync_set(client_target,sync_end)
        cmd = f"""
        ip link set mtu 1500 dev {nic_test}
        ip addr flush {nic_test}
        """
        run(cmd)
        vf_create_from_pf(nic_test,0)
    else:
        cmd = f"""
        ip link set mtu 9000 dev {nic_test}
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr}.2/24 dev {nic_test}
        ip addr add 2021:db8:{ipaddr}::2/64 dev {nic_test}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip link set mtu 1500 dev {nic_test}
        ip addr flush {nic_test}
        """
        run(cmd)
    pass

def sriov_test_pf_vlan_remote():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    vid = 3
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    modprobe -r 8021q
    """
    run(cmd)
    if i_am_server():
        sync_wait(client_target,sync_start)
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        cmd = f"""
        ip link add link {nic_test} name {nic_test}.{vid} type vlan id {vid}
        ip link set {nic_test}.{vid} up
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr_vlan}.1/24 dev {nic_test}.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::1/64 dev {nic_test}.{vid}
        ip -d link show {nic_test}.{vid}
        ip -d addr show {nic_test}.{vid}
        """
        run(cmd)

        do_host_iperf(f"172.30.{ipaddr_vlan}.2",f"2021:db8:{ipaddr_vlan}::2") 

        cmd = f"""
        ip link set {nic_test}.{vid} down
        ip link del {nic_test}.{vid}
        """
        run(cmd)

        vf_create_from_pf(nic_test,0)

        sync_set(client_target,sync_end)
    else:
        cmd = f"""
        ip link add link {nic_test} name {nic_test}.{vid} type vlan id {vid}
        ip link set {nic_test}.{vid} up
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr_vlan}.2/24 dev {nic_test}.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::2/64 dev {nic_test}.{vid}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip link set {nic_test}.{vid} down
        ip link del {nic_test}.{vid}
        """
        run(cmd)
    pass

def sriov_test_vf_remote():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    vid = 3
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    """
    run(cmd)
    if i_am_server():
        sync_wait(client_target,sync_start)
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        vf = vf_list[0]
        vf_mac = my_tool.get_mac_from_name(vf)
        random_mac = my_tool.get_random_mac_addr()
        cmd = f"""
        ethtool -i {vf}
        """
        run(cmd)
        if vf_mac == "00:00:00:00:00:00":
            cmd = f"""
            ip link set {nic_test} vf 0 mac {random_mac}
            #workaround for ionic bz1914175
            ip link set dev {vf} address {random_mac}
            ip link set {vf} down
            sleep 2
            """
            run(cmd)
        cmd = f"""
        ip link set {vf} up
        sleep 5
        ip addr flush {vf}
        ip addr add 172.30.{ipaddr}.1/24 dev {vf}
        ip addr add 2021:db8:{ipaddr}::1/64 dev {vf}
        ip -d link show {vf}
        ip -d addr show {vf}
        """
        run(cmd)

        do_host_iperf(f"172.30.{ipaddr}.2",f"2021:db8:{ipaddr}::2")

        cmd = f"""
        ip addr flush {vf}
        """
        run(cmd)
        vf_create_from_pf(nic_test,0)
        sync_set(client_target,sync_end)
    else:
        cmd = f"""
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr}.2/24 dev {nic_test}
        ip addr add 2021:db8:{ipaddr}::2/64 dev {nic_test}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"ip addr flush {nic_test}"
        run(cmd)
    pass

def sriov_test_vf_remote_jumbo():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    vid = 3
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    """
    run(cmd)
    if i_am_server():
        sync_wait(client_target,sync_start)
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        vf = vf_list[0]
        vf_mac = my_tool.get_mac_from_name(vf)
        random_mac = my_tool.get_random_mac_addr()
        cmd = f"""
        ethtool -i {vf}
        """
        run(cmd)
        if vf_mac == "00:00:00:00:00:00":
            cmd = f"""
            ip link set {nic_test} vf 0 mac {random_mac}
            #workaround for ionic bz1914175
            ip link set dev {vf} address {random_mac}
            ip link set {vf} down
            sleep 2
            """
            run(cmd)
        cmd = f"""
        ip link set {vf} up
        sleep 10
        ip link set mtu 9000 dev {nic_test}
        sleep 10
        ip link set mtu 9000 dev {vf}
        sleep 10
        ip addr flush {vf}
        sleep 1
        ip addr add 172.30.{ipaddr}.1/24 dev {vf}
        sleep 1
        ip addr add 2021:db8:{ipaddr}::1/64 dev {vf}
        sleep 1
        ip -d link show {vf}
        ip -d addr show {vf}
        """
        run(cmd)

        do_host_iperf(f"172.30.{ipaddr}.2",f"2021:db8:{ipaddr}::2")

        cmd = f"""
        ip link set mtu 1500 dev {vf}
        ip link set mtu 1500 dev {nic_test}
        ip addr flush {vf}
        """
        run(cmd)
        vf_create_from_pf(nic_test,0)
        sync_set(client_target,sync_end)
    else:
        cmd = f"""
        ip link set mtu 9000 dev {nic_test}
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr}.2/24 dev {nic_test}
        ip addr add 2021:db8:{ipaddr}::2/64 dev {nic_test}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip addr flush {nic_test}
        ip link set mtu 1500 dev {nic_test}
        """
        run(cmd)
    pass

def sriov_test_vf_vlan_remote():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    vid = 3
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    modprobe -r 8021q
    """
    run(cmd)
    if i_am_server():
        sync_wait(client_target,sync_start)
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        vf = vf_list[0]
        vf_mac = my_tool.get_mac_from_name(vf)
        random_mac = my_tool.get_random_mac_addr()
        cmd = f"""
        ethtool -i {vf}
        """
        run(cmd)
        if vf_mac == "00:00:00:00:00:00":
            cmd = f"""
            ip link set {nic_test} vf 0 mac {random_mac}
            #workaround for ionic bz1914175
            ip link set dev {vf} address {random_mac}
            ip link set {vf} down
            sleep 2
            """
            run(cmd)
        cmd = f"""
        ip link set {vf} up
        sleep 5
        ip link add link {vf} name {vf}.{vid} type vlan id {vid}
        ip link set {vf}.{vid} up
        ip addr flush {vf}
        ip addr add 172.30.{ipaddr_vlan}.1/24 dev {vf}.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::1/64 dev {vf}.{vid}
        ip link show {vf}.{vid}
        ip addr show {vf}.{vid}
        """
        run(cmd)

        do_host_iperf(f"172.30.{ipaddr_vlan}.2",f"2021:db8:{ipaddr_vlan}::2")

        cmd = f"""
        ip link set {vf}.{vid} down
        ip link del {vf}.{vid}
        """
        run(cmd)
        vf_create_from_pf(nic_test,0)
        sync_set(client_target,sync_end)
    else:
        cmd = f"""
        ip link add link {nic_test} name {nic_test}.{vid} type vlan id {vid}
        ip link set {nic_test}.{vid} up
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr_vlan}.2/24 dev {nic_test}.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::2/64 dev {nic_test}.{vid}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip link set {nic_test}.{vid} down
        ip link del {nic_test}.{vid}
        """
        run(cmd)
    pass

def sriov_test_vmvf_remote():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    """
    run(cmd)
    if i_am_server():
        sync_wait(client_target,sync_start)
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        vf = vf_list[0]
        vf_mac = my_tool.get_mac_from_name(vf)
        log(f"Init vf mac : {vf_mac}")
        vm_mac = my_tool.get_random_mac_addr()
        log(f"vf vm mac : {vm_mac}")
        cmd = f"""
        ethtool -i {vf}
        """
        run(cmd)
        vf_bus = ethtool.get_businfo(vf)
        pysriov.attach_vf_to_vm(vf,vm1,vm_mac)
        log(f"{func_name} show vm after attach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        """
        run(cmd)
        cmd = f"""
        export NIC_TEST=$(ip link show | grep {vm_mac} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip addr add 172.30.{ipaddr}.11/24 dev $NIC_TEST
        ip addr add 2021:db8:{ipaddr}::11/64 dev $NIC_TEST
        ip link show $NIC_TEST
        ip addr show $NIC_TEST
        """
        log(cmd)
        vm_run_cmd(vm1,cmd)

        do_vm_iperf(vm1,f"172.30.{ipaddr}.2",f"2021:db8:{ipaddr}::2")

        #detach vf from vm
        sriov_deatch_vf_from_vm(vm1,vf_bus)
        log(f"{func_name} show vm after detach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        """
        run(cmd)
        #clear vf 
        vf_create_from_pf(nic_test,0)

        sync_set(client_target,sync_end)
    else:
        cmd = f"""
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr}.2/24 dev {nic_test}
        ip addr add 2021:db8:{ipaddr}::2/64 dev {nic_test}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip addr flush {nic_test}
        """
        run(cmd)

# vlan interface created on VF in VM
def sriov_test_vmvf_vlan_remote():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    vid = 3
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    modprobe -r 8021q
    """
    run(cmd)
    if i_am_server():
        sync_wait(client_target,sync_start)
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return

        vf = vf_list[0]
        vf_mac = my_tool.get_mac_from_name(vf)
        log(f"Init vf mac : {vf_mac}")
        vm_mac = my_tool.get_random_mac_addr()
        log(f"vf vm mac : {vm_mac}")
        cmd = f"""
        ethtool -i {vf}
        """
        run(cmd)
        vf_bus = ethtool.get_businfo(vf)
        pysriov.attach_vf_to_vm(vf,vm1,vm_mac)
        log(f"{func_name} show vm after attach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        """
        run(cmd)

        cmd = f"""
        modprobe -r 8021q
        export NIC_TEST=$(ip link show | grep {vm_mac} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip link add link $NIC_TEST name $NIC_TEST.{vid} type vlan id {vid}
        ip link set $NIC_TEST.{vid} up
        ip addr flush $NIC_TEST.{vid}
        ip addr add 172.30.{ipaddr_vlan}.11/24 dev $NIC_TEST.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::11/64 dev $NIC_TEST.{vid}
        ip link show $NIC_TEST.{vid}
        ip addr show $NIC_TEST.{vid}
        """
        vm_run_cmd(vm1,cmd)

        do_vm_iperf(vm1,f"172.30.{ipaddr_vlan}.2",f"2021:db8:{ipaddr_vlan}::2")

        #detach vf from vm
        sriov_deatch_vf_from_vm(vm1,vf_bus)
        log(f"{func_name} show vm after detach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        """
        run(cmd)
        #clear vf 
        vf_create_from_pf(nic_test,0)
        sync_set(client_target,sync_end)
    else:
        cmd = f"""
        ip addr flush {nic_test}
        ip link add link {nic_test} name {nic_test}.{vid} type vlan id {vid}
        ip link set {nic_test}.{vid} up
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr_vlan}.2/24 dev {nic_test}.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::2/64 dev {nic_test}.{vid}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip link set {nic_test}.{vid} down
        ip link del {nic_test}.{vid}
        ip addr flush {nic_test}
        """
        run(cmd)
    pass


def sriov_test_vmvf_vlan_remote_jumbo():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    vid = 3
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    modprobe -r 8021q
    """
    run(cmd)
    if i_am_server():
        sync_wait(client_target,sync_start)
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        vf = vf_list[0]
        vf_mac = my_tool.get_mac_from_name(vf)
        log(f"Init vf mac : {vf_mac}")
        vm_mac = my_tool.get_random_mac_addr()
        log(f"vf vm mac : {vm_mac}")
        cmd = f"""
        ethtool -i {vf}
        """
        run(cmd)
        vf_bus = ethtool.get_businfo(vf)
        pysriov.attach_vf_to_vm(vf,vm1,vm_mac)
        log(f"{func_name} show vm after attach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        """
        run(cmd)

        cmd = f"""
        modprobe -r 8021q
        export NIC_TEST=$(ip link show | grep {vm_mac} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip link set mtu 9000 dev $NIC_TEST
        sleep 5
        ip link add link $NIC_TEST name $NIC_TEST.{vid} type vlan id {vid}
        ip link set $NIC_TEST.{vid} up
        ip link set mtu 9000 dev $NIC_TEST.{vid}
        ip addr flush $NIC_TEST.{vid}
        ip addr add 172.30.{ipaddr_vlan}.11/24 dev $NIC_TEST.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::11/64 dev $NIC_TEST.{vid}
        ip link show $NIC_TEST.{vid}
        ip addr show $NIC_TEST.{vid}
        """
        vm_run_cmd(vm1,cmd)

        do_vm_iperf(vm1,f"172.30.{ipaddr_vlan}.2",f"2021:db8:{ipaddr_vlan}::2")

        #detach vf from vm
        sriov_deatch_vf_from_vm(vm1,vf_bus)
        log(f"{func_name} show vm after detach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        """
        run(cmd)
        #clear vf 
        vf_create_from_pf(nic_test,0)
        sync_set(client_target,sync_end)
    else:
        cmd = f"""
        ip addr flush {nic_test}
        ip link set mtu 9000 {nic_test}
        ip link add link {nic_test} name {nic_test}.{vid} type vlan id {vid}
        ip link set {nic_test}.{vid} up
        ip link set mtu 9000 {nic_test}.{vid}
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr_vlan}.2/24 dev {nic_test}.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::2/64 dev {nic_test}.{vid}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip link set {nic_test}.{vid} down
        ip link del {nic_test}.{vid}
        ip addr flush {nic_test}
        """
        run(cmd)
    pass

# set vlan in XML for 'virsh attach-device'
def sriov_test_vmvf_vlan_remote_1():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    vid = 3
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    modprobe -r 8021q
    """
    run(cmd)
    if i_am_server():
        sync_wait(client_target,sync_start)
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        vf = vf_list[0]
        vf_mac = my_tool.get_mac_from_name(vf)
        log(f"Init vf mac : {vf_mac}")
        vm_mac = my_tool.get_random_mac_addr()
        log(f"vf vm mac : {vm_mac}")
        cmd = f"""
        ethtool -i {vf}
        """
        run(cmd)
        vf_bus = ethtool.get_businfo(vf)
        pysriov.sriov_attach_vf_to_vm_with_hostdev(vf,vm1,vm_mac)
        log(f"{func_name} show vm after attach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        """
        run(cmd)

        cmd = f"""
        modprobe -r 8021q
        export NIC_TEST=$(ip link show | grep {vm_mac} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip link add link $NIC_TEST name $NIC_TEST.{vid} type vlan id {vid}
        ip link set $NIC_TEST.{vid} up
        ip addr flush $NIC_TEST.{vid}
        ip addr add 172.30.{ipaddr_vlan}.11/24 dev $NIC_TEST.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::11/64 dev $NIC_TEST.{vid}
        ip link show $NIC_TEST.{vid}
        ip addr show $NIC_TEST.{vid}
        """
        vm_run_cmd(vm1,cmd)

        do_vm_iperf(vm1,f"172.30.{ipaddr_vlan}.2",f"2021:db8:{ipaddr_vlan}::2")

        #detach vf from vm
        sriov_deatch_vf_from_vm(vm1,vf_bus)
        log(f"{func_name} show vm after detach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        """
        run(cmd)
        #clear vf 
        vf_create_from_pf(nic_test,0)
        sync_set(client_target,sync_end)
    else:
        cmd = f"""
        ip addr flush {nic_test}
        ip link add link {nic_test} name {nic_test}.{vid} type vlan id {vid}
        ip link set {nic_test}.{vid} up
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr_vlan}.2/24 dev {nic_test}.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::2/64 dev {nic_test}.{vid}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip link set {nic_test}.{vid} down
        ip link del {nic_test}.{vid}
        ip addr flush {nic_test}
        """
        run(cmd)
    pass

def sriov_test_pf_vmvf():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    vid = 3
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    """
    run(cmd)
    if i_am_server():
        sync_wait(client_target,sync_start)
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        vf = vf_list[0]
        vf_mac = my_tool.get_mac_from_name(vf)
        log(f"Init vf mac : {vf_mac}")
        mac = f"00:de:ad:{ipaddr:02x}:01:01"
        log(f"set mac is {mac}")
        vf_bus = ethtool.get_businfo(vf)
        pysriov.sriov_attach_vf_to_vm(vf,vm1,mac)
        log(f"{func_name} show vm after attach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        """
        run(cmd)
        #pf setup
        cmd = f"""
        ip link set {nic_test} up
        ip addr flush {nic_test}
        ip addr add 172.30.{ipaddr}.1/24 dev {nic_test}
        ip addr add 2021:db8:{ipaddr}::1/64 dev {nic_test}
        ip -d link show {nic_test}
        ip -d addr show {nic_test}
        """
        run(cmd)

        cmd = f"""
        iptables -F
        ip6tables -F
        systemctl stop firewalld
        sleep 3
        export NIC_TEST=$(ip link show | grep {mac} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip addr add 172.30.{ipaddr}.11/24 dev $NIC_TEST
        ip addr add 2021:db8:{ipaddr}::11/64 dev $NIC_TEST
        ip link show $NIC_TEST
        ip addr show $NIC_TEST
        """
        vm_run_cmd(vm1,cmd)
        vm_restart_iperf(vm1)

        do_host_iperf(f"172.30.{ipaddr}.11",f"2021:db8:{ipaddr}::11")
        do_vm_iperf(vm1,f"172.30.{ipaddr}.1",f"2021:db8:{ipaddr}::1")

        #detach vf from vm
        sriov_deatch_vf_from_vm(vm1,vf_bus)
        log(f"{func_name} show vm after detach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        """
        run(cmd)
        cmd = f"""
        ip addr flush {nic_test}
        """
        run(cmd)
        #clear vfs
        vf_create_from_pf(nic_test,0)
        sync_set(client_target,sync_end)
    else:
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
    pass

def sriov_test_pf_vmvf_vlan():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    vid = 3
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    modprobe -r 8021q
    """
    run(cmd)
    if i_am_server():
        sync_wait(client_target,sync_start)
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        vf = vf_list[0]
        vf_mac = my_tool.get_mac_from_name(vf)
        log(f"Init vf mac : {vf_mac}")
        mac = f"00:de:ad:{ipaddr:02x}:01:01"
        log(f"set mac is {mac}")
        vf_bus = ethtool.get_businfo(vf)
        pysriov.sriov_attach_vf_to_vm(vf,vm1,mac)
        log(f"{func_name} show vm after attach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        """
        run(cmd)
        #pf setup
        cmd = f"""
        ip link set {nic_test} up
        ip addr flush {nic_test}
        ip link add link {nic_test} name {nic_test}.{vid} type vlan id {vid}
        ip link set {nic_test}.{vid} up
        ip addr flush {nic_test}.{vid}
        ip addr add 172.30.{ipaddr_vlan}.1/24 dev {nic_test}.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::1/64 dev {nic_test}.{vid}
        ip -d link show {nic_test}.{vid}
        ip -d addr show {nic_test}.{vid}
        """
        run(cmd)

        cmd = f"""
        modprobe -r 8021q
        iptables -F
        ip6tables -F
        systemctl stop firewalld
        sleep 3
        export NIC_TEST=$(ip link show | grep {mac} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip link add link $NIC_TEST name $NIC_TEST.{vid} type vlan id {vid}
        ip link set $NIC_TEST.{vid} up
        ip addr flush $NIC_TEST.{vid}
        ip addr add 172.30.{ipaddr_vlan}.11/24 dev $NIC_TEST.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::11/64 dev $NIC_TEST.{vid}
        ip link show $NIC_TEST.{vid}
        ip addr show $NIC_TEST.{vid}
        """
        vm_run_cmd(vm1,cmd)
        vm_restart_iperf(vm1)

        do_host_iperf(f"172.30.{ipaddr_vlan}.11",f"2021:db8:{ipaddr_vlan}::11")
        do_vm_iperf(vm1,f"172.30.{ipaddr_vlan}.1",f"2021:db8:{ipaddr_vlan}::1")

        #detach vf from vm
        sriov_deatch_vf_from_vm(vm1,vf_bus)
        log(f"{func_name} show vm after detach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        """
        run(cmd)
        cmd = f"""
        ip link set {nic_test}.{vid} down
        ip link del {nic_test}.{vid}
        ip addr flush {nic_test}
        """
        run(cmd)
        #clear vfs
        vf_create_from_pf(nic_test,0)
        sync_set(client_target,sync_end)
    else:
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
    pass

# two VFs on same PF
def sriov_test_vmvf_vmvf():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    vid = 3
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    """
    run(cmd)
    if i_am_server():
        sync_wait(client_target,sync_start)
        mac1 = f"00:de:ad:{ipaddr:02x}:01:01"
        mac2 = f"00:de:ad:{ipaddr:02x}:02:01"
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        vf1 = vf_list[0]
        vf2 = vf_list[1]
        vf1_mac = my_tool.get_mac_from_name(vf1)
        vf2_mac = my_tool.get_mac_from_name(vf2)
        log(f"Init vf mac : {vf1_mac}")
        log(f"Init vf mac : {vf2_mac}")
        vf1_bus = ethtool.get_businfo(vf1)
        vf2_bus = ethtool.get_businfo(vf2)
        pysriov.sriov_attach_vf_to_vm(vf1,vm1,mac1)
        pysriov.sriov_attach_vf_to_vm(vf2,vm2,mac2)
        log(f"{func_name} show vm after attach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        virsh dumpxml {vm2}
        """
        run(cmd)

        cmd = f"""
        iptables -F
        ip6tables -F
        systemctl stop firewalld
        sleep 3
        """
        vm_run_cmd(vm1,cmd)
        vm_run_cmd(vm2,cmd)
        vm_restart_iperf(vm1)
        vm_restart_iperf(vm2)

        cmd = f"""
        export NIC_TEST=$(ip link show | grep {mac1} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip addr add 172.30.{ipaddr}.11/24 dev $NIC_TEST
        ip addr add 2021:db8:{ipaddr}::11/64 dev $NIC_TEST
        ip link show $NIC_TEST
        ip addr show $NIC_TEST
        """
        vm_run_cmd(vm1,cmd)

        cmd = f"""
        export NIC_TEST=$(ip link show | grep {mac2} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip addr add 172.30.{ipaddr}.21/24 dev $NIC_TEST
        ip addr add 2021:db8:{ipaddr}::21/64 dev $NIC_TEST
        ip link show $NIC_TEST
        ip addr show $NIC_TEST
        """
        vm_run_cmd(vm2,cmd)

        do_vm_iperf(vm1,f"172.30.{ipaddr}.21",f"2021:db8:{ipaddr}::21")
        do_vm_iperf(vm2,f"172.30.{ipaddr}.11 ",f"2021:db8:{ipaddr}::11")
        do_vm_iperf(vm1,f"172.30.{ipaddr}.1 ",f"2021:db8:{ipaddr}::1")
        do_vm_iperf(vm2,f"172.30.{ipaddr}.1",f"2021:db8:{ipaddr}::1")

        sriov_deatch_vf_from_vm(vm1,vf1_bus)
        sriov_deatch_vf_from_vm(vm2,vf2_bus)
        log(f"{func_name} show vm after detach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        virsh dumpxml {vm2}
        """
        run(cmd)

        vf_create_from_pf(nic_test,0)
        sync_set(client_target,sync_end)
    else:
        cmd = f"""
        ip link set {nic_test} up
        ip addr add 172.30.{ipaddr}.1/24 dev {nic_test}
        ip addr add 2021:db8:{ipaddr}::1/64 dev {nic_test}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip addr flush {nic_test}
        """
        run(cmd)
    pass

# two VFs on same PF
def sriov_test_vmvf_vmvf_vlan():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    vid = 3
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    modprobe -r 8021q
    """
    run(cmd)
    if i_am_server():
        sync_wait(client_target,sync_start)
        mac1 = f"00:de:ad:{ipaddr:02x}:01:01"
        mac2 = f"00:de:ad:{ipaddr:02x}:02:01"
        vf_list = vf_create_from_pf(nic_test,2)
        log(f"all vf list {vf_list}")
        if len(vf_list) == 0:
            sync_set(client_target,sync_end)
            rl_fail(f"{func_name} create vf on {nic_test} failed")
            return
        vf1 = vf_list[0]
        vf2 = vf_list[1]
        vf1_mac = my_tool.get_mac_from_name(vf1)
        vf2_mac = my_tool.get_mac_from_name(vf2)
        log(f"Init vf mac : {vf1_mac}")
        log(f"Init vf mac : {vf1_mac}")
        vf1_bus = ethtool.get_businfo(vf1)
        vf2_bus = ethtool.get_businfo(vf2)
        pysriov.sriov_attach_vf_to_vm(vf1,vm1,mac1)
        pysriov.sriov_attach_vf_to_vm(vf2,vm2,mac2)
        log(f"{func_name} show vm after attach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        virsh dumpxml {vm2}
        """
        run(cmd)

        cmd = f"""
        iptables -F
        ip6tables -F
        systemctl stop firewalld
        sleep 3
        """
        vm_run_cmd(vm1,cmd)
        vm_run_cmd(vm2,cmd)
        vm_restart_iperf(vm1)
        vm_restart_iperf(vm2)

        cmd = f"""
        modprobe -r 8021q
        export NIC_TEST=$(ip link show | grep {mac1} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip link add link $NIC_TEST name $NIC_TEST.{vid} type vlan id {vid}
        ip link set $NIC_TEST.{vid} up
        ip addr flush $NIC_TEST.{vid}
        ip addr add 172.30.{ipaddr_vlan}.11/24 dev $NIC_TEST.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::11/64 dev $NIC_TEST.{vid}
        ip link set $NIC_TEST.{vid} up
        ip link show $NIC_TEST.{vid}
        ip addr show $NIC_TEST.{vid}
        """
        vm_run_cmd(vm1,cmd)

        cmd = f"""
        modprobe -r 8021q
        export NIC_TEST=$(ip link show | grep {mac2} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip link add link $NIC_TEST name $NIC_TEST.{vid} type vlan id {vid}
        ip link set $NIC_TEST.{vid} up
        ip addr flush $NIC_TEST.{vid}
        ip addr add 172.30.{ipaddr_vlan}.21/24 dev $NIC_TEST.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::21/64 dev $NIC_TEST.{vid}
        ip link set $NIC_TEST.{vid} up
        ip link show $NIC_TEST.{vid}
        ip addr show $NIC_TEST.{vid}
        """
        vm_run_cmd(vm2,cmd)

        do_vm_iperf(vm1,f"172.30.{ipaddr_vlan}.21",f"2021:db8:{ipaddr_vlan}::21")
        do_vm_iperf(vm2,f"172.30.{ipaddr_vlan}.11",f"2021:db8:{ipaddr_vlan}::11")
        do_vm_iperf(vm1,f"172.30.{ipaddr_vlan}.1",f"2021:db8:{ipaddr_vlan}::1")
        do_vm_iperf(vm2,f"172.30.{ipaddr_vlan}.1",f"2021:db8:{ipaddr_vlan}::1")

        sriov_deatch_vf_from_vm(vm1,vf1_bus)
        sriov_deatch_vf_from_vm(vm2,vf2_bus)
        log(f"{func_name} show vm after detach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        virsh dumpxml {vm2}
        """
        run(cmd)
        vf_create_from_pf(nic_test,0)
        sync_set(client_target,sync_end)
    else:
        cmd = f"""
        ip link set {nic_test} up
        ip link add link {nic_test} name {nic_test}.{vid} type vlan id {vid}
        ip link set {nic_test}.{vid} up
        ip addr add 172.30.{ipaddr_vlan}.1/24 dev {nic_test}.{vid}
        ip addr add 2021:db8:{ipaddr_vlan}::1/64 dev {nic_test}.{vid}
        ip link set {nic_test}.{vid} up
        ip addr show {nic_test}.{vid}
        """
        run(cmd)
        host_restart_iperf()
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip link del {nic_test}.{vid}
        """
        run(cmd)
    pass

#two vfs from different pfs on one vm
def sriov_test_vmvf1vf2_remote():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    client_ip4 = f"192.100.{ipaddr}.1"
    vf1_ip4 = f"192.100.{ipaddr}.2"
    vf2_ip4 = f"192.101.{ipaddr}.2"
    client_ip6 = f"2021:db08:{ipaddr}:2345::1"
    vf1_ip6 = f"2021:db08:{ipaddr}:2345::2"
    vf2_ip6 = f"2021:db09:{ipaddr}:2345::2"
    ip4_mask_len = 24
    ip6_mask_len = 64
    pf2_ip4 = f"192.101.{ipaddr}.1"
    vid = 3
    mac1 = f"00:de:a1:{ipaddr:02x}:11:01"
    mac2 = f"00:de:a1:{ipaddr:02x}:12:01"
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    ip addr flush {nic_test}
    """
    run(cmd)
    if i_am_client():
        cmd = f"""
        ip addr add {client_ip4}/{ip4_mask_len} dev {nic_test}
        ip addr add {client_ip6}/{ip6_mask_len} dev {nic_test}
        ip route add 192.101.{ipaddr}.0/{ip4_mask_len} dev {nic_test}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
    else:
        sync_wait(client_target,sync_start)
        iface1 = get_env("SERVER_NIC1")
        iface2 = get_env("SERVER_NIC2")
        if nic_test == iface1:
            pass
        else:
            iface1,iface2 = iface2,iface1
        log(f"iface1 : {iface1} and iface2 : {iface2}")
        cmd = f"""
        ip link set {iface1} up
        ip link set {iface2} up
        ip addr flush {iface1}
        ip addr flush {iface2}
        sleep 1
        ip addr add {pf2_ip4}/{ip4_mask_len} dev {nic_test}
        ip route add 192.100.{ipaddr}.0/{ip4_mask_len} dev {nic_test}
        ip addr show {nic_test}
        ip route
        """
        run(cmd)
        vf1_list = vf_create_from_pf(iface1,2)
        vf2_list = vf_create_from_pf(iface2,2)
        log(f"all vf1 list {vf1_list}")
        log(f"all vf2 list {vf2_list}")
        if len(vf1_list) == 0 or len(vf2_list) == 0:
            rl_fail(f"{func_name} create vf on {iface1} or {iface2} failed")
            vf_create_from_pf(iface1,0)
            vf_create_from_pf(iface2,0)
            sync_set(client_target,sync_end)
            return
        cmd = f"""
        ip link set {iface1} up
        ip link set {iface2} up
        """
        run(cmd)
        cmd = f"""
        iptables -F
        ip6tables -F
        systemctl stop firewalld
        """
        vm_run_cmd(vm1,cmd)
        vm_restart_iperf(vm1)
        vf1 = vf1_list[0]
        vf2 = vf2_list[1]
        log(f"vf1 : {vf1} vf2 : {vf2}")
        vf1_bus = ethtool.get_businfo(vf1)
        vf2_bus = ethtool.get_businfo(vf2)
        log(f"vf1_bus : {vf1_bus} vf2_bus : {vf2_bus}")
        pysriov.attach_vf_to_vm(vf1,vm1,mac1)
        pysriov.attach_vf_to_vm(vf2,vm2,mac2)
        log(f"{func_name} show vm after attach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        virsh dumpxml {vm2}
        """
        run(cmd)
        #set vm cmds
        cmd = f"""
        export NIC_TEST1=$(ip link show | grep {mac1} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST1 up
        ip addr flush $NIC_TEST1
        ip addr add {vf1_ip4}/{ip4_mask_len} dev $NIC_TEST1
        ip addr add {vf1_ip6}/{ip6_mask_len} dev $NIC_TEST1
        ip addr show $NIC_TEST1
        """
        vm_run_cmd(vm1,cmd)

        cmd = f"""
        export NIC_TEST2=$(ip link show | grep {mac2} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST2 up
        ip addr flush $NIC_TEST2
        ip addr add {vf2_ip4}/{ip4_mask_len} dev $NIC_TEST2
        ip addr add {vf2_ip6}/{ip6_mask_len} dev $NIC_TEST2
        ip addr show $NIC_TEST2
        sysctl -w net.ipv4.ip_forward=1
        sysctl -w net.ipv6.conf.default.forwarding=1
        """
        vm_run_cmd(vm2,cmd)

        do_vm_iperf(vm1,client_ip4,client_ip6)

        cmd = f"""
        ping {client_ip4} -c 5
        ip addr flush {iface1}
        ip addr flush {iface2}
        """
        run(cmd)
        sriov_deatch_vf_from_vm(vm1,vf1_bus)
        sriov_deatch_vf_from_vm(vm2,vf2_bus)
        log(f"{func_name} show vm after detach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        virsh dumpxml {vm2}
        """
        run(cmd)
        vf_create_from_pf(iface1,0)
        vf_create_from_pf(iface2,0)
        sync_set(client_target,sync_end)

# two VFs on different PF
def sriov_test_vmvf1_vmvf2_remote():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    client_ip4 = f"192.100.{ipaddr}.1"
    vm1_ip4 = f"192.100.{ipaddr}.2"
    vm2_ip4 = f"192.100.{ipaddr}.3"
    client_ip6 = f"2021:db08:{ipaddr}:2345::1"
    vm1_ip6 = f"2021:db08:{ipaddr}:2345::2"
    vm2_ip6 = f"2021:db08:{ipaddr}:2345::3"
    ip4_mask_len = 24
    ip6_mask_len = 64
    mac1 = f"00:de:a1:{ipaddr:02x}:11:01"
    mac2 = f"00:de:a1:{ipaddr:02x}:12:01"
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    ip addr flush {nic_test}
    """
    run(cmd)
    if i_am_client():
        cmd = f"""
        ip addr flush {nic_test}
        ip addr add {client_ip4}/{ip4_mask_len} dev {nic_test}
        ip addr add {client_ip6}/{ip6_mask_len} dev {nic_test}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip addr flush {nic_test}
        """
        run(cmd)
    else:
        sync_wait(client_target,sync_start)
        iface1 = get_env("SERVER_NIC1")
        iface2 = get_env("SERVER_NIC2")
        if nic_test == iface1:
            pass
        else:
            iface1,iface2 = iface2,iface1
        log(f"iface1 : {iface1} and iface2 : {iface2}")
        cmd = f"""
        ip link set {iface1} up
        ip link set {iface2} up
        ip addr flush {iface1}
        ip addr flush {iface2}
        """
        run(cmd)
        vf1_list = vf_create_from_pf(iface1,2)
        vf2_list = vf_create_from_pf(iface2,2)
        log(f"all vf1 list {vf1_list}")
        log(f"all vf2 list {vf2_list}")
        if len(vf1_list) == 0 or len(vf2_list) == 0:
            rl_fail(f"{func_name} create vf on {iface1} or {iface2} failed")
            vf_create_from_pf(iface1,0)
            vf_create_from_pf(iface2,0)
            sync_set(client_target,sync_end)
            return
        cmd = f"""
        ip link set {iface1} up
        ip link set {iface2} up
        """
        run(cmd)
        vf1 = vf1_list[0]
        vf2 = vf2_list[1]
        vf1_bus = ethtool.get_businfo(vf1)
        vf2_bus = ethtool.get_businfo(vf2)
        pysriov.attach_vf_to_vm(vf1,vm1,mac1)
        pysriov.attach_vf_to_vm(vf2,vm2,mac2)
        log(f"{func_name} show vm after attach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        virsh dumpxml {vm2}
        """
        run(cmd)
        cmd = f"""
        iptables -F
        ip6tables -F
        systemctl stop firewalld
        """
        vm_run_cmd(vm1,cmd)
        vm_run_cmd(vm2,cmd)
        vm_restart_iperf(vm1)
        vm_restart_iperf(vm2)

        cmd = f"""
        export NIC_TEST=$(ip link show | grep {mac1} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip addr add {vm1_ip4}/{ip4_mask_len} dev $NIC_TEST
        ip addr add {vm1_ip6}/{ip6_mask_len} dev $NIC_TEST
        ip addr show $NIC_TEST
        """
        vm_run_cmd(vm1,cmd)

        cmd = f"""
        export NIC_TEST=$(ip link show | grep {mac2} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip addr add {vm2_ip4}/{ip4_mask_len} dev $NIC_TEST
        ip addr add {vm2_ip6}/{ip6_mask_len} dev $NIC_TEST
        ip addr show $NIC_TEST
        """
        vm_run_cmd(vm2,cmd)
        do_vm_iperf(vm1,client_ip4,client_ip6)
        do_vm_iperf(vm2,vm1_ip4,vm1_ip6)

        # clearnup
        sriov_deatch_vf_from_vm(vm1,vf1_bus)
        sriov_deatch_vf_from_vm(vm2,vf2_bus)
        log(f"{func_name} show vm after detach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        virsh dumpxml {vm2}
        """
        run(cmd)
        vf_create_from_pf(iface1,0)
        vf_create_from_pf(iface2,0)
        sync_set(client_target,sync_end)
    pass

# two VFs on different PF
def sriov_test_vmvf1_vmvf2_vlan_remote():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    client_ip4 = f"192.100.{ipaddr}.1"
    vm1_ip4 = f"192.100.{ipaddr}.2"
    vm2_ip4 = f"192.100.{ipaddr}.3"
    client_ip6 = f"2021:db08:{ipaddr}:1234::1"
    vm1_ip6 = f"2021:db08:{ipaddr}:1234::2"
    vm2_ip6 = f"2021:db08:{ipaddr}:1234::3"
    ip4_mask_len = 24
    ip6_mask_len = 64
    vlan_id = 3
    mac1 = f"00:de:ad:{ipaddr:02x}:01:01"
    mac2 = f"00:de:ad:{ipaddr:02x}:02:01"
    cmd = f"""
    ip link set {nic_test} up
    ip li show {nic_test}
    ip addr flush {nic_test}
    modprobe -r 8021q
    """
    run(cmd)
    if i_am_client():
        cmd = f"""
        ip link add link {nic_test} name {nic_test}.{vlan_id} type vlan id {vlan_id}
        ip link set {nic_test}.{vlan_id} up
        ip addr add {client_ip4}/{ip4_mask_len} dev {nic_test}.{vlan_id}
        ip addr add {client_ip6}/{ip6_mask_len} dev {nic_test}.{vlan_id}
        """
        run(cmd)
        sync_set(server_target,sync_start)
        sync_wait(server_target,sync_end)
        cmd = f"""
        ip link del {nic_test}.{vlan_id}
        ip addr flush {nic_test}
        """
        run(cmd)
    else:
        sync_wait(client_target,sync_start)
        iface1 = get_env("SERVER_NIC1")
        iface2 = get_env("SERVER_NIC2")
        if nic_test == iface1:
            pass
        else:
            iface1,iface2 = iface2,iface1
        log(f"iface1 : {iface1} and iface2 : {iface2}")
        cmd = f"""
        ip link set {iface1} up
        ip link set {iface2} up
        ip addr flush {iface1}
        ip addr flush {iface2}
        """
        run(cmd)
        
        vf1_list = vf_create_from_pf(iface1,2)
        vf2_list = vf_create_from_pf(iface2,2)
        log(f"all vf1 list {vf1_list}")
        log(f"all vf2 list {vf2_list}")
        if len(vf1_list) == 0 or len(vf2_list) == 0:
            rl_fail(f"{func_name} create vf on {iface1} or {iface2} failed")
            vf_create_from_pf(iface1,0)
            vf_create_from_pf(iface2,0)
            sync_set(client_target,sync_end)
            return
        cmd = f"""
        ip link set {iface1} up
        ip link set {iface2} up
        """
        run(cmd)
        vf1 = vf1_list[0]
        vf2 = vf2_list[1]
        vf1_bus = ethtool.get_businfo(vf1)
        vf2_bus = ethtool.get_businfo(vf2)
        pysriov.attach_vf_to_vm(vf1,vm1,mac1)
        pysriov.attach_vf_to_vm(vf2,vm2,mac2)
        log(f"{func_name} show vm after attach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        virsh dumpxml {vm2}
        """
        run(cmd)
        cmd = f"""
        iptables -F
        ip6tables -F
        systemctl stop firewalld
        """
        vm_run_cmd(vm1,cmd)
        vm_run_cmd(vm2,cmd)
        vm_restart_iperf(vm1)
        vm_restart_iperf(vm2)

        cmd = f"""
        modprobe -r 8021q
        export NIC_TEST=$(ip link show | grep {mac1} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip link add link $NIC_TEST name $NIC_TEST.{vlan_id} type vlan id {vlan_id}
        ip link set $NIC_TEST.{vlan_id} up
        ip addr add {vm1_ip4}/{ip4_mask_len} dev $NIC_TEST.{vlan_id}
        ip addr add {vm1_ip6}/{ip6_mask_len} dev $NIC_TEST.{vlan_id}
        ip addr show $NIC_TEST.{vlan_id}
        """
        vm_run_cmd(vm1,cmd)
        cmd = f"""
        modprobe -r 8021q
        export NIC_TEST=$(ip link show | grep {mac2} -B1 | head -n1 | awk '{{print $2}}' | sed 's/://')
        ip link set $NIC_TEST up
        ip addr flush $NIC_TEST
        ip link add link $NIC_TEST name $NIC_TEST.{vlan_id} type vlan id {vlan_id}
        ip link set $NIC_TEST.{vlan_id} up
        ip addr add {vm2_ip4}/{ip4_mask_len} dev $NIC_TEST.{vlan_id}
        ip addr add {vm2_ip6}/{ip6_mask_len} dev $NIC_TEST.{vlan_id}
        ip addr show $NIC_TEST.{vlan_id}
        """
        vm_run_cmd(vm2,cmd)
        do_vm_iperf(vm1,client_ip4,client_ip6)
        do_vm_iperf(vm2,{vm1_ip4},vm1_ip6)

        # clearnup
        sriov_deatch_vf_from_vm(vm1,vf1_bus)
        sriov_deatch_vf_from_vm(vm2,vf2_bus)
        log(f"{func_name} show vm after detach vf")
        cmd = f"""
        virsh dumpxml {vm1}
        virsh dumpxml {vm2}
        """
        run(cmd)
        vf_create_from_pf(iface1,0)
        vf_create_from_pf(iface2,0)
        sync_set(client_target,sync_end)
    pass

def run_functional_test():
    with enter_phase("sriov_test_pf_remote"):
        sriov_test_pf_remote()
        pass
    with enter_phase("sriov_test_pf_remote_jumbo"):
        sriov_test_pf_remote_jumbo()
        pass
    with enter_phase("sriov_test_pf_vlan_remote"):
        sriov_test_pf_vlan_remote()
        pass
    with enter_phase("sriov_test_vf_remote"):
        sriov_test_vf_remote()
        pass
    with enter_phase("sriov_test_vf_remote_jumbo"):
        sriov_test_vf_remote_jumbo()
        pass
    with enter_phase("sriov_test_vf_vlan_remote"):
        sriov_test_vf_vlan_remote()
        pass
    with enter_phase("sriov_test_vmvf_remote"):
        sriov_test_vmvf_remote()
        pass
    with enter_phase("sriov_test_vmvf_vlan_remote"):
        sriov_test_vmvf_vlan_remote()
        pass
    with enter_phase("sriov_test_vmvf_vlan_remote_jumbo"):
        sriov_test_vmvf_vlan_remote_jumbo()
        pass
    with enter_phase("sriov_test_vmvf_vlan_remote_1"):
        sriov_test_vmvf_vlan_remote_1()
        pass
    with enter_phase("sriov_test_pf_vmvf"):
        sriov_test_pf_vmvf()
        pass
    with enter_phase("sriov_test_pf_vmvf_vlan"):
        sriov_test_pf_vmvf_vlan()
        pass
    with enter_phase("sriov_test_vmvf_vmvf"):
        sriov_test_vmvf_vmvf()
        pass
    with enter_phase("sriov_test_vmvf_vmvf_vlan"):
        sriov_test_vmvf_vmvf_vlan()
        pass
    with enter_phase("sriov_test_vmvf1vf2_remote"):
        sriov_test_vmvf1vf2_remote()
        pass
    with enter_phase("sriov_test_vmvf1_vmvf2_remote"):
        sriov_test_vmvf1_vmvf2_remote()
        pass
    with enter_phase("sriov_test_vmvf1_vmvf2_vlan_remote"):
        sriov_test_vmvf1_vmvf2_vlan_remote()
        pass

def run_throughput_test():
    sriov_pci_passthrough_test_wrap(1,64,60)
    sriov_pci_passthrough_test_wrap(1,1500,60)
    pass

def main(test_list="ALL"):
    if test_list == "FUNCTIONAL_TEST" or test_list == "ALL":
        with enter_phase("SRIOV ENV SETUP"):
            setup()
            pass
        with enter_phase("run sriov functional test"):
            run_functional_test()
            pass
#    if test_list == "THROUGHPUT_TEST" or test_list == "ALL":
#        with enter_phase("run sriov throughput test"):
#            run_throughput_test()
#            pass
