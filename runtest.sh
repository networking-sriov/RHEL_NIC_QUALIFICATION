#!/bin/bash
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Description: bash scripts entrance for NIC certification
#   Author: Hekai Wang <hewang@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
dbg_flag=${dbg_flag:-"set +x"}
#output function
con_print_green() {
    local RED='\033[0;31m'
    local Green='\033[0;32m'
    local Blue='\033[0;34m'
    local NC='\033[0m'
    echo -e "${Green}$1${NC}"
}
con_print_red() {
    local RED='\033[0;31m'
    local Green='\033[0;32m'
    local Blue='\033[0;34m'
    local NC='\033[0m'
    echo -e "${RED}$1${NC}"
}
con_print_blue() {
    local RED='\033[0;31m'
    local Green='\033[0;32m'
    local Blue='\033[0;34m'
    local NC='\033[0m'
    echo -e "${Blue}$1${NC}"
}
# Detect OS name and version from systemd based os-release file
init_env() {
    set -a
    CASE_PATH="$(dirname "$(readlink -f "$0")")"
    source /etc/os-release
    SYSTEM_VERSION_ID=$(echo $VERSION_ID | tr -d '.')
    SYSTEM_MAJOR_VERSION=$(echo $VERSION_ID | awk -F '.' '{print $1}')
    ALL_CMD_FILE="/var/tmp/all_commands"
    CMD_FILE="/var/tmp/shell_commands"
    LOCK_FILE="/var/tmp/rhel-nic-cert-lock-file"
    MAIN_FILE="nic_cert_main.py"
    set +a
    con_print_red "############################################################"
    con_print_green "CASE_PATH : ${CASE_PATH}"
    con_print_green "SYSTEM_VERSION_ID : ${SYSTEM_VERSION_ID}"
    con_print_green "SYSTEM_MAJOR_VERSION : ${SYSTEM_MAJOR_VERSION}"
    con_print_green "ALL_CMD_FILE : ${ALL_CMD_FILE}"
    con_print_green "CMD_FILE : ${CMD_FILE}"
    con_print_green "LOCK_FILE : ${LOCK_FILE}"
    con_print_green "MAIN_FILE : ${MAIN_FILE}"
    con_print_red "############################################################"
}

trap ctrl_c INT
ctrl_c() {
    kill -TERM 0 && wait
}

check_install(){
    local pkg_name=$1
    rpm -q ${pkg_name} || yum -y install ${pkg_name}
}

install_python() {
    #install epel repo
    yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-"${SYSTEM_MAJOR_VERSION}".noarch.rpm
    # yum clean all
    yum makecache

    if ((SYSTEM_VERSION_ID < 80)); then
        yum -y install python-netifaces
        yum -y install python2-devel
    else
        yum -y install python3-netifaces
        yum -y install platform-python-devel
    fi

    yum -y install python3
    yum -y install python3-devel
    yum -y install python3-pyelftools
    yum -y install python3-pip
    if ((SYSTEM_VERSION_ID < 80)); then
        yum -y install python2
        yum -y install python-pip
        python2 -m pip install --upgrade pip
        python2 -m pip install wheel
        python2 -m pip install netifaces
        python2 -m pip install six
    fi
}

install_python_and_init_env() {
    install_python
    pushd "$CASE_PATH"
    if ((SYSTEM_VERSION_ID > 76)); then
        python3 -m venv "${CASE_PATH}"/venv
    else
        python36 -m venv "${CASE_PATH}"/venv
    fi
    yum -y install vim

    source venv/bin/activate
    pip install --upgrade pip
    pip install wheel
    pip install inquirerpy
    pip install fire
    pip install psutil
    pip install paramiko
    pip install xmlrunner
    pip install netifaces
    pip install argparse
    pip install plumbum
    pip install ethtool
    pip install shell
    pip install libvirt-python
    pip install envbash
    pip install bash
    pip install pexpect
    pip install serial
    pip install pyserial
    pip install napalm
    pip install remote-pdb
    pip install ripdb
    pip install scapy
    pip install openpyxl
    pip install Pillow
    pip install tee
    pip install lxml
    pip install requests
    deactivate
    popd
}

install_beakerlib() {
    pushd "$CASE_PATH"
    check_install gcc
    check_install git
    check_install make

    test -f /usr/share/beakerlib/beakerlib.sh && return 0
    test -d beakerlib && return 0

    git clone https://github.com/beakerlib/beakerlib.git
    pushd beakerlib
    git checkout 1.29.3
    make install
    popd

    popd
    return 0
}

install_rpms() {
    check_install iperf3
    iperf_path=`which iperf3`
    test -f "${iperf_path}" && ln -s ${iperf_path} /usr/bin/iperf
    check_install libnl3-devel
    check_install libvirt-devel
    check_install telnet
    check_install procmail
    check_install yum-utils
    check_install scl-utils
    check_install tuned-profiles-cpu-partitioning
    check_install wget
    check_install wget 
    check_install nano 
    check_install ftp 
    check_install git 
    check_install tuna 
    check_install openssl 
    check_install sysstat
    check_install libvirt 
    check_install virt-install 
    check_install virt-manager 
    check_install virt-viewer
    check_install vim
    check_install pciutils
    check_install libnl3-devel
    check_install driverctl
    check_install czmq-devel
    check_install libguestfs-tools
    check_install ethtool
    if (( SYSTEM_MAJOR_VERSION >= 80 ));then
        check_install python3-lxml
    fi
    check_install qemu-kvm
    #for qemu bug that can not start qemu
    echo -e "group = 'hugetlbfs'" >>/etc/libvirt/qemu.conf
    if (( SYSTEM_MAJOR_VERSION < 70 )); then
        service libvirtd restart
    else
        systemctl restart libvirtd
        systemctl start virtlogd.socket
    fi
}

clean_previous_nic_cert_main_test() {
    local my_pid=$$
    echo "my pid is "$my_pid
    local my_child_pid=$(pgrep -P $my_pid)
    echo "child pid $my_child_pid"
    local run_pid=$(pgrep -f ${MAIN_FILE})
    echo "${MAIN_FILE} pid list $run_pid"
    for i in $run_pid; do
        if grep -q "$i" <<<"${my_child_pid[@]}"; then
            continue
        elif [[ $i == "$my_pid" ]]; then
            continue
        else
            kill ${i} 2> /dev/null && wait ${i} 2> /dev/null
        fi
    done
}

start_main_process() {
    pushd "$CASE_PATH"
    echo "Begin Start python process"
    source venv/bin/activate
    python -u ${MAIN_FILE}
    deactivate
    sleep 3
    popd
}

all_env_init() {
    init_env
    install_rpms
    install_beakerlib
    install_python_and_init_env
    sleep 3
    source common/lib_nc_sync.sh || exit 1
    source common/lib_utils.sh || exit 1
    source /usr/share/beakerlib/beakerlib.sh || exit 1
    set -a
    source nic_cert.conf || exit 1
    set +a
}

start_run_test() {
    all_env_init
    clean_previous_nic_cert_main_test

    rm -f ${LOCK_FILE}
    rm -f ${ALL_CMD_FILE}
    rm -f ${CMD_FILE}

    touch ${ALL_CMD_FILE}
    touch ${CMD_FILE}

    start_main_process &
    sleep 5

    #read command from cmd file
    local N=0
    while true; do
        if test ! -s ${CMD_FILE}; then
            sleep 0.1
            N=$((N + 1))
            if [[ $N == 6000 ]]; then
                echo "${CMD_FILE} NOT UPDATE IN 600 SECONDS .NOW QUIT"
                break
            fi
        else
            N=0
            lockfile ${LOCK_FILE}
            if grep rhel-nic-cert-gitlab-quit-string ${CMD_FILE}; then
                kill -TERM 0 && wait
                break
            fi
            source ${CMD_FILE}
            cat ${CMD_FILE} >>${ALL_CMD_FILE}
            true >${CMD_FILE}
            rm -f ${LOCK_FILE}
        fi
    done
}

start_run_test
